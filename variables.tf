variable "name_prefix" {
  type        = string
  description = "Name for prefix in naming convention of all resources. Default: sample-batch"
  default     = "submit-batch-job"
}

variable "mandatory_tags" {
  description = "Mapping of tags to add to all taggable resources"
  default     = {}
}

variable "schedule_rate" {
  type        = number
  description = "The rate (in minutes) of how often to submit the Batch Job. Default: 15"
  default     = 15
}

variable "rule_is_enabled" {
  type        = bool
  description = "Boolean of whether or not the Rule is currently active. Default: true"
  default     = true
}

variable "job_definition_arn" {
  type        = string
  description = "ARN of the Batch Job Definition."
}

variable "job_queue_arn" {
  type        = string
  description = "ARN of the Batch Job Queue."
}

variable "job_name" {
  type        = string
  description = "Name for the Batch Job that is submitted. Default: name_prefix"
  default     = ""
}

variable "retry_job_attempts" {
  type        = number
  description = "Number of times to retry the job if it fails. 1 to 10. Default: 1"
  default     = 1
}
