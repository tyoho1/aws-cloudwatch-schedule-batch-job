resource "aws_cloudwatch_event_rule" "schedule_batch_event" {
  name        = "${var.name_prefix}EventRule"
  description = "Start a Batch Job on a fixed schedule"
  is_enabled  = var.rule_is_enabled

  schedule_expression = "rate(${tostring(var.schedule_rate)} minutes)"

  tags = var.mandatory_tags
}

resource "aws_cloudwatch_event_target" "submit_batch_job" {
  arn      = var.job_queue_arn
  rule     = aws_cloudwatch_event_rule.schedule_batch_event.name
  role_arn = aws_iam_role.event_service_role.arn

  batch_target {
    job_definition = var.job_definition_arn
    job_name       = var.job_name == "" ? var.name_prefix : var.job_name
    job_attempts   = var.retry_job_attempts
  }
}

resource "aws_iam_role" "event_service_role" {
  name = "${var.name_prefix}EventServiceRole"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
    {
        "Action": "sts:AssumeRole",
        "Effect": "Allow",
        "Principal": {
        "Service": "events.amazonaws.com"
        }
    }
    ]
}
EOF

  tags = var.mandatory_tags
}

resource "aws_iam_role_policy_attachment" "event_service_role_policy" {
  role       = aws_iam_role.event_service_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBatchServiceEventTargetRole"
}
