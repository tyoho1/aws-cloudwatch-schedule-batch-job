# aws-cloudwatch-schedule-batch-job

---
A terraform module to create a CloudWatch Event to submit a Batch Job on a fixed schedule.

This module is best used with my other module to create a complete Batch Job at <https://bitbucket.org/tyoho1/aws-batch-complete>

## Optional Module Input Variables

---
|Name|Type|Default|Description|
|:---:|:---:|:---:|---|
|name_prefix|string|`"submit-batch-job"`|Name for prefix in naming convention of all resources.|
|mandatory_tags|mapping of tags|`{}`|Mapping of tags to add to all taggable resources|
|schedule_rate|number|`15`|The rate (in minutes) of how often to submit the Batch Job.|
|rule_is_enabled|bool|`true`|Boolean of whether or not the Rule is currently active.|
|job_definition_arn|string||ARN of the Batch Job Definition.|
|job_queue_arn|string||ARN of the Batch Job Queue.|
|job_name|string|name_prefix|Name for the Batch Job that is submitted.|
|retry_job_attempts|number|`1`|Number of times to retry the job if it fails. 1 to 10.|

## Usage

---

### Standalone

```text
module "schedule_batch_job" {
  source = "bitbucket.org/tyoho1/aws-cloudwatch-schedule-batch-job"
  name_prefix = "schedule-my-custom-batch-job"
  schedule_rate = 60
  job_definition_arn = aws_batch_job_definition.main.arn
  job_queue_arn = aws_batch_job_queue.main.arn

  mandatory_tags = {
    Costcenter = "CC12345"
    Environment = "DEV"
  }
}
```

### With Batch Module

```text
module "schedule_batch_job" {
  source = "bitbucket.org/tyoho1/aws-cloudwatch-schedule-batch-job"
  name_prefix = "schedule-my-custom-batch-job"
  schedule_rate = 1440
  rule_is_enabled = true
  job_definition_arn = module.batch_complete.batch_job_definition_arn
  job_queue_arn = module.batch_complete.batch_job_queue_arn
  job_name = "cw-scheduled-job"
  retry_job_attempts = 2

  mandatory_tags = {
    Costcenter = "CC12345"
    Environment = "DEV"
  }
}

module "batch_complete" {
  source = "bitbucket.org/tyoho1/aws-batch-complete"
  name_prefix = "myCustomBatch"
  vpc_id = "vpc-123"
  instance_policies = [aws_iam_role_policy.custom_instance_policy.arn]

  mandatory_tags = {
      Costcenter = "CC12345"
      Environment = "DEV"
  }

  container_properties = <<CONTAINER_PROPERTIES
  {
    "command": ["ls", "-la"],
    "image": "busybox",
    "memory": 1024,
    "vcpus": 1,
    "volumes": [
      {
        "host": {
          "sourcePath": "/tmp"
        },
        "name": "tmp"
      }
    ],
    "environment": [
        {"name": "VARNAME", "value": "VARVAL"}
    ],
    "mountPoints": [
        {
          "sourceVolume": "tmp",
          "containerPath": "/tmp",
          "readOnly": false
        }
    ],
    "ulimits": [
      {
        "hardLimit": 1024,
        "name": "nofile",
        "softLimit": 1024
      }
    ]
}
  CONTAINER_PROPERTIES
}
```

## Outputs

---

- `cloudwatch_rule_name` - Name of the CloudWatch Event Rule
- `cloudwatch_rule_arn` - ARN of the CloudWatch Event Rule

## Authors

---
<tyoho@hgsdigital.com>
